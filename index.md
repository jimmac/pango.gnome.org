---
layout: default
---
<picture class="full pixels">
    <source srcset="assets/splash-dark.png" media="(prefers-color-scheme: dark)">
    <img src="assets/splash.png">
</picture>


Pango is a library for laying out and rendering of text, with an emphasis on internationalization. Pango can be used anywhere that text layout is needed, though most of the work on Pango so far has been done in the context of the GTK widget toolkit. Pango forms the core of text and font handling for GTK.

Pango is designed to be modular; the core Pango layout engine can be used with different font backends. There are three basic backends, with multiple options for rendering with each.

- Client side fonts using the FreeType and fontconfig libraries. Rendering can be with Cairo or Xft libraries, or directly to an in-memory buffer with no additional libraries.
- Native fonts on Microsoft Windows. Rendering can be done via Cairo or directly using the native Win32 API.
- Native fonts on MacOS X, rendering via [Cairo](https://jimmac.github.io/cairographics.org/). 

Complex-text support is provided by HarfBuzz, on all platforms.

The integration of Pango with Cairo provides a complete solution with high quality text handling and graphics rendering.

As well as the low-level layout rendering routines, Pango includes PangoLayout, a high-level driver for laying out entire blocks of text, and routines to assist in editing internationalized text.

Pango depends on 2.x series of the GLib library; more information about GLib can be found at: https://www.gtk.org/

If this information is insufficient, please see [the documentation](https://docs.gtk.org/Pango/).

## Downloads

Building Pango from source can at times be a difficult process.

If there are packages of Pango available from your operating system vendor, you should use those packages. Virtually all distributions of Linux install Pango by default; in fact, if you are using Linux, Pango may well be rendering the text you are reading at the moment.

Trying to install a newer copy of Pango along side your operating system's version is especially challenging. If you want to do this, we would recommend using JHBuild.

### Source releases

Releases of Pango are available from:

[https://download.gnome.org/sources/pango/](https://download.gnome.org/sources/pango/) 

For information about the requirements of Pango, see the Pango [README](https://gitlab.gnome.org/GNOME/pango/-/blob/main/README.md?ref_type=heads) file.

### Development tree

The Pango source code is available from GNOME's GitLab. You can clone the source code repository using:

```
git clone https://gitlab.gnome.org/GNOME/pango.git
```

The easiest and recommended way to build Pango from the repository is to use meson. It handles automatically downloading and building the dependencies of Pango as subprojects.

### Contributing

If you need help using Pango, please open a new topic on [GNOME's Discourse](https://discourse.gnome.org/tag/pango), using the `pango` tag.

#### Reporting bugs

Pango bugs should be reported to the project's [GitLab page](https://gitlab.gnome.org/GNOME/pango/issues/new). GitLab is also where we keep track of planned features and post patches.
